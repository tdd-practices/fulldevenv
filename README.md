# Teljes fejlesztői környezet #

Ez a projekt tartalmazza a teljes fejlesztői környetet, amelyet [docker](https://www.docker.com/)-el lehet elindítani.

***

A futtatáshoz először le kell fordítani a jenkins Dockerfile-t a __jenkins__ mappában kiadva a következő parancsot:
```bash
docker build
```

A teljes környezetet ezután a projekt mappájában az alábbi paranccsal lehet elindítani:
```bash
docker-compose up -d
```

Ez elindítja a környezetet daemon módban, tehát a háttérben fog futni.

A logokat így lehet megnézni a futó környezetekről:
```bash
docker-compose logs -f
```

Ez követi a log változásokat, kilépni ebből a módból CTRL+C -vel lehet.

A Jenkins beállításához a logba fog generálni egy jelszót az első belépéskor, az összes logból ezt nem egyszerű előkeresni, ezért van lehetőség csak a jenkins logját is megnézni:
```bash
docker-compose logs jenkins
```

Leállítani a containereket pedig így lehet:
```bash
docker-compose stop
```